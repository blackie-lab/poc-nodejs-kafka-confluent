# poc-nodejs-kafka-confluent

PoC of nodejs and kafka confluent

## Getting started

![kafka_connector_v2](images/kafka_connector_v2.jpeg))

source: 將 Kafka 視為訂閱方、接收各種不同類型資料庫的原始資料變化收進 Kafka 指定 Topic 內
sink:: 將 Kafka 視為推送方、將topic內收到的新訊息往指定的資料庫位置做新增、修改

實際上的 source 與 sink 有不同的種類、就會搭配不同的設定，參考會如下：

![kafka_connector](images/kafka_connector.png))

## Kafka Connector Example

示範官方 mongodb 與 kafka-connector 如何溝通與互動

範例位置： `lab/kafka-connector-quickstart`

```sh
#前往指定範例
cd lab/kafka-connector-quickstart
#啟用範例
docker-compose -p quickstart up -d

#進入shell(有kafkacat, mongosh)
docker exec -it shell /bin/bash

#完整關閉且清除範例
docker-compose -p quickstart down --rmi 'all'

#確認 connector 設定
curl -X GET http://connect:8083/connectors

```

## mongosh

```sh
#進入 mongo instance
mongosh mongodb://mongo1:27017/?replicaSet=rs0

#切換到指定 db
use quickstart

#查詢指定 collection 
db.sampleData.find()
db.topicData.find()

#新增 sampleData
db.sampleData.insertOne({"hello":"hihi","test":123,"gg":{"key":123}})

```

## kafkacat

```sh
#列出 Kafka topic
kafkacat -L -b broker:9092

#訂閱 Kafka topic
kafkacat -C -b broker:29092 -t quickstart.sampleData

#發送訊息到指定的 Kafka topic
kafkacat -b broker:29092 -t quickstart.sampleData -P
```

發送訊息的範本：

```sh
#這邊要記得換掉 _id 內的數值，因為目前是針對 Insert 且 _id 為唯一，如果不更換則會導致訊息都無法正確塞入 mongodb 且在 kafka 內因為失敗沒有 commit 會重複執行
{"schema":{"type":"string","optional":false},"payload":"{\"_id\": {\"_data\": \"82625636B0000000012B022C0100296E5A10049CC0C5D6E19842D1906AA9EF5B653AD346645F69640064625636B03DE753EB667E8CE7003ˋ\"}, \"operationType\": \"insert\", \"clusterTime\": {\"$timestamp\": {\"t\": 1659817264, \"i\": 1}}, \"fullDocument\": {\"_id\": {\"$oid\": \"625636b03de753eb667e8ceb\"}, \"hello\": \"hihi\", \"test\": 123, \"gg\": {\"key\": 123}, \"travel\": \"MongoDB Kafka Connector\"}, \"ns\": {\"db\": \"quickstart\", \"coll\": \"sampleData\"}, \"documentKey\": {\"_id\": {\"$oid\": \"625636b03de753eb667e8ceb\"}}}"}
```

## Express + React with mongodb and kafka connector

示範 Express 如何完整的讀出 mongodb 並且自動進行畫面更新

TBD

## References

- [MongoDB Kafka Connector - Quick Start](https://www.mongodb.com/docs/kafka-connector/current/quick-start/)
- [kafka调试工具kafkacat的使用](https://www.cnblogs.com/xdao/p/10674848.html)
- [kafkacat Utility](https://docs.confluent.io/3.3.0/app-development/kafkacat-usage.html)