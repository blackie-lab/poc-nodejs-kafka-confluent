const express = require('express')
const fs = require('fs');
const path = require('path');
const { MongoClient } = require('mongodb');

// init
const app = express()

// Config
const port = process.env.PORT || 3000;
const config = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'config.json')));

const connectionSetting = {
  url : config.connectionSetting.url,
  db : config.connectionSetting.db,
  collection:config.connectionSetting.collection
}

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

// Replace the uri string with your MongoDB deployment's connection string.
const client = new MongoClient(connectionSetting.url);
const simulateAsyncPause = () =>
  new Promise(resolve => {
    setTimeout(() => resolve(), 1000);
  });
let changeStream;
async function run() {
  try {
    await client.connect();
    const database = client.db(connectionSetting,db);
    const collection = database.collection(connectionSetting.collection);
    // open a Change Stream on the "haikus" collection
    changeStream = collection.watch();
    // set up a listener when change events are emitted
    changeStream.on("change", next => {
      // process any change event
      console.log("received a change to the collection: \t", next);
    });
    await simulateAsyncPause();
    await collection.insertOne({
      title: "Record of a Shriveled Datum",
      content: "No bytes, no problem. Just insert a document, in MongoDB",
    });
    await simulateAsyncPause();
    await changeStream.close();
    
    console.log("closed the change stream");
  } finally {
    await client.close();
  }
}
run().catch(console.dir);